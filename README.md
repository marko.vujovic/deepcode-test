1. install dependencies
`$ npm install`

2. login
https://www.deepcode.ai/cloud-login

3. run application
`$ node index.js`
