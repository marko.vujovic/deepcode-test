const crypto = require('crypto');
const fs = require('fs');
const path = require('path');
const https = require('https')
const open = require('open');

const hostName = 'www.deepcode.ai';
const filePath = path.join(__dirname, 'index.js');

async function run() {

  // read file content
  const fileContent = await new Promise((resolve, reject) => {
    const readStream = fs.createReadStream(filePath, 'utf8');
    let data = '';
    readStream
      .on('data', chunk => data += chunk)
      .on('end', () => resolve(data));
  });

  // create file hash
  const fileHash = crypto.createHash('sha256').update(fileContent).digest('hex');

  // login
  const {sessionToken, loginURL} = await new Promise((resolve, reject) => {
    const options = {
      hostname: hostName,
      path: '/publicapi/login',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const req = https.request(options, res => {
      res.setEncoding('utf-8');
      res.setEncoding('utf8');
      res.on('data', d => resolve(JSON.parse(d)));
    })
    req.on('error', reject);
    req.write('');
    req.end();
  });
  await open(loginURL);

  // create bundle
  const {bundleId, uploadURL} = await new Promise((resolve, reject) => {
    const options = {
      hostname: hostName,
      path: '/publicapi/bundle',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Session-Token': sessionToken
      }
    }
    const req = https.request(options, res => {
      res.on('data', d => resolve(JSON.parse(d)));
    })
    req.on('error', reject);
    setTimeout(() => {
      const data = JSON.stringify({
        'files': {
          [filePath]: fileHash
        }
      });
      req.write(data);
      req.end();
    }, 1000)
  });

  // upload files
  const r = await new Promise((resolve, reject) => {
    const options = {
      hostname: hostName,
      path: `/publicapi/file/${bundleId}`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Session-Token': sessionToken
      }
    }
    const req = https.request(options, res => {
      res.on('data', d => resolve(d));
    })
    req.on('error', reject);
    setTimeout(() => {
      const data = JSON.stringify({
        fileHash: fileHash,
        fileContent: fileContent
      });
      req.write(data);
      req.end();
    }, 1000)
  });

  // call API
  async function analyze() {
    const r = await new Promise((resolve, reject) => {
      const options = {
        hostname: hostName,
        path: `/publicapi/analysis/${bundleId}`,
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Session-Token': sessionToken
        }
      }
      const req = https.request(options, res => {
        res.setEncoding('utf-8');
        res.setEncoding('utf8');
        const chunks = [];
        res.on('data', c => chunks.push(c));
        res.on('end', () => {
          const d = chunks.join('');
          const data = JSON.parse(d);
          if (data.status === 'DONE') {
            resolve(data);
          } else {
            setTimeout(() => resolve(analyze()), 1000);
          }
        });
      })
      req.on('error', reject);
      req.end();
    });
    return r;
  }

  const {analysisURL, analysisResults} = await analyze();
  console.log(analysisResults);
  open(analysisURL);

}
// test
new Promise((resolve, reject) => setTimeout(() => resolve('test'), 10)).then(console.log);

run();

// https://stackoverflow.com/a/46801928
// https://stackoverflow.com/questions/27970431/using-sha-256-with-nodejs-crypto
// https://github.com/DeepCodeAI/vscode-extension
